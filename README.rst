==========
mp3gaintag
==========

A simple program that writes ReplayGain tags to an MP3 file without modifying
the file data itself.

------------
Dependencies
------------
- mp3gain for calculating gain data
- id3 for writing tags
