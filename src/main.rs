/* Copyright 2017 Dmitri Kourennyi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

extern crate clap;
extern crate regex;

use std::process::Command;

use clap::{Arg, App};
use regex::Regex;

fn main() {
    let matches = App::new("mp3gainonly")
        .arg(Arg::with_name("INPUT")
                 .help("MP3 file to analyze+tag")
                 .required(true))
        .arg(Arg::with_name("dry-run")
                 .short("n")
                 .long("dry-run")
                 .help("Don't modfiy files, only print new gain and peak values. Implies verbose."))
        .arg(Arg::with_name("verbose")
                 .short("v")
                 .long("verbose")
                 .help("Show values of new tags."))
        .arg(Arg::with_name("show-old")
                 .short("o")
                 .long("show-old")
                 .help("Show existing track ReplayGain tags, if present."))
        .get_matches();

    let file = matches.value_of("INPUT").unwrap();

    let output = Command::new("mp3gain")
        .arg("-s")
        .arg("s")
        .arg(file)
        .output()
        .expect("failed to execute process");

    let analysis = String::from_utf8_lossy(&output.stdout);

    let gain_re = Regex::new(r#"Recommended "Track" dB change:\s+(-?[\d]+.?[\d]+)$"#).unwrap();
    let peak_re = Regex::new(r"Max PCM sample at current gain:\s+(-?[\d]+.?[\d]+)$").unwrap();

    let mut gain: Option<f64> = None;
    let mut peak: Option<f64> = None;

    for line in analysis.lines() {
        gain_re
            .captures(&line)
            .map(|x| match x.get(1).unwrap().as_str().parse::<f64>() {
                     Ok(new_gain) => gain = Some(new_gain),
                     _ => {}
                 });
        peak_re
            .captures(&line)
            .map(|x| match x.get(1).unwrap().as_str().parse::<f64>() {
                     Ok(new_peak) => peak = Some(new_peak / 32768.0),
                     _ => {}
                 });
    }

    if gain.is_none() || peak.is_none() {
        panic!(format!("Failed to peak({:?}) or gain({:?})", peak, gain));
    }

    let gain = gain.unwrap();
    let peak = peak.unwrap();

    if gain <= -100.0 || gain >= 100.0 {
        panic!(format!("Gain exceeds specification with a value of {:?}", gain));
    }

    if peak < 0.0 || peak >= 10.0 {
        panic!(format!("Peak exceeds specification with a value of {:?}", peak));
    }

    let gain = format!("{:.2} dB", gain);
    let peak = format!("{:.6}", peak);

    if matches.is_present("show-old") {
        let old_gain = Command::new("id3")
            .arg("-q")
            .arg("%|%{TXXX:REPLAYGAIN_TRACK_GAIN}||%{TXXX:replaygain_track_gain}|?")
            .arg(file)
            .output()
            .expect("failed to execute process")
            .stdout;

        let old_peak = Command::new("id3")
            .arg("-q")
            .arg("%|%{TXXX:REPLAYGAIN_TRACK_PEAK}||%{TXXX:replaygain_track_peak}|?")
            .arg(file)
            .output()
            .expect("failed to execute process")
            .stdout;

        println!("Old gain is {:} and peak is {:}.",
                 String::from_utf8_lossy(&old_gain).trim(),
                 String::from_utf8_lossy(&old_peak).trim());
    }

    if matches.is_present("dry-run") || matches.is_present("verbose") {
        println!("New gain is {:} and peak is {:}.", gain, peak);
    }

    if matches.is_present("dry-run") {
        std::process::exit(0);
    }

    // id3 provides its own unneeded globbing, so fix file paths with brackets
    // in them before sending them off.
    let file_fix_re = Regex::new("\\[").unwrap();
    let file_fixed = file_fix_re.replace_all(file, "[[]");

    let output = Command::new("id3")
        .arg("-2")
        .arg("--remove=TXXX:replaygain_track_gain")
        .arg("--frame=TXXX:REPLAYGAIN_TRACK_GAIN")
        .arg(gain)
        .arg("--remove=TXXX:replaygain_track_peak")
        .arg("--frame=TXXX:REPLAYGAIN_TRACK_PEAK")
        .arg(peak)
        .arg(file_fixed.as_ref())
        .output()
        .unwrap();

    if !output.status.success() {
        panic!(format!("Writing tags failed with output:\n{:}",
                       String::from_utf8_lossy(&output.stderr)));
    }
}
